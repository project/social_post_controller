<?php

namespace Drupal\social_post_controller\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a SocialPlatform annotation object.
 *
 * @see \Drupal\social_post_controller\SocialPlatformPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
class SocialPlatform extends Plugin
{
  /**
   * Provides a label for the posting mechanism.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * Provides mechanism for posting content to a third party social platform.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

  /**
   * The maximum number of characters allowed on the target third party social platform.
   *
   * @var int
   */
  public $max_char_count;

}
