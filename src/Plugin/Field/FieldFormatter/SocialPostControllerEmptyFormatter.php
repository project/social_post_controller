<?php

namespace Drupal\social_post_controller\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'social_post_controller_empty_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "social_post_controller_empty_formatter",
 *   module = "social_post_controller",
 *   label = @Translation("Empty formatter"),
 *   field_types = {
 *     "social_post_controller"
 *   }
 * )
 */
class SocialPostControllerEmptyFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    // Does not actually output anything.
    return [];
  }

}
