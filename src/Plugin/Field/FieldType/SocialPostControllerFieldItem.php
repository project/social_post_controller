<?php

namespace Drupal\social_post_controller\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'social_post_controller' field type.
 *
 * @FieldType(
 *   id = "social_post_controller",
 *   label = @Translation("Social post controller"),
 *   description = @Translation("This field stores whether or not to post to various social media platforms on publish."),
 *   list_class = "\Drupal\social_post_controller\Plugin\Field\FieldType\SocialPostControllerFieldItemList",
 *   default_widget = "social_post_controller_toggles",
 *   default_formatter = "social_post_controller_empty_formatter",
 *   module = "social_post_controller",
 * )
 */
class SocialPostControllerFieldItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'platform' => [
          'type' => 'text',
          'not null' => FALSE,
        ],
        'message' => [
          'type' => 'text',
          'not null' => FALSE,
        ],
        'status' => [
          'type' => 'text',
          'not null' => FALSE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName() {
    return 'platform';
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['platform'] = DataDefinition::create('string')
      ->setLabel(t('Platform'));

    $properties['message'] = DataDefinition::create('string')
      ->setLabel(t('Message'));

    $properties['status'] = DataDefinition::create('string')
      ->setLabel(t('Status'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('platform')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  // public function preSave() {
  //   parent::preSave();

  //   // Merge field defaults on top of global ones.
  //   // $default_statuses = social_post_controller_get_default_statuses($this->getEntity());

  //   // Get the value about to be saved.
  //   $current_value = $this->platforms;
  //   // Only unserialize if still serialized string.
  //   if (is_string($current_value)) {
  //     $current_statuses = unserialize($current_value, ['allowed_classes' => FALSE]);
  //   }
  //   else {
  //     $current_statuses = $current_value;
  //   }

  //   // Sort the values prior to saving. so that they are easier to manage.
  //   ksort($current_statuses);

  //   // Update the value to only save overridden tags.
  //   $this->platforms = serialize($current_statuses);
  // }

}
