<?php

namespace Drupal\social_post_controller\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\social_post_controller\SocialPostControllerManagerInterface;
use Drupal\social_post_controller\SocialPlatformPluginManager;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Advanced widget for social_post_controller field.
 *
 * @FieldWidget(
 *   id = "social_post_controller_toggles",
 *   label = @Translation("Social post controller toggles"),
 *   field_types = {
 *     "social_post_controller"
 *   }
 * )
 */
class SocialPostControllerToggles extends WidgetBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  const DISABLED = 'disabled';
  const OFF = 'off';
  const ON = 'on';

  /**
   * Instance of SocialPostControllerManager service.
   *
   * @var \Drupal\social_post_controller\SocialPostControllerManagerInterface
   */
  protected $socialPostControllerManager;

  /**
   * Instance of SocialPlatformPluginManager service.
   *
   * @var \Drupal\social_post_controller\SocialPlatformPluginManager
   */
  protected $socialPlatformPluginManager;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a SocialPostControllerToggles object.
   *
   * @param string $plugin_id
   *   The plugin_id for the widget.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\social_post_controller\SocialPlatformPluginManager $plugin_manager
   *   The social platform plugin manager.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, SocialPlatformPluginManager $plugin_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->socialPlatformPluginManager = $plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('plugin.manager.social_platform'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'platforms' => [],
      'sidebar' => TRUE,
      'use_details' => TRUE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    // @TODO add configuration settings for each platform.
    $plugin_manager = $this->socialPlatformPluginManager;
    $plugin_definitions = $plugin_manager->getDefinitions();
    $platforms = $this->getSetting('platforms');
    foreach ($plugin_definitions as $plugin_id => $definition) {
      $platform_status = isset($platforms[$plugin_id]) ? $platforms[$plugin_id] : NULL;
      $element['platforms'][$plugin_id]['status'] = [
        '#type' => 'select',
        '#title' => $definition['label'],
        '#description' => $definition['description'],
        '#default_value' => $platform_status ?: self::DISABLED,
        '#options' => [
          self::DISABLED => 'Disabled',
          self::OFF => 'Off by default',
          self::ON => 'On by default',
        ],
      ];
    }


    $element['sidebar'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Place field in sidebar'),
      '#default_value' => $this->getSetting('sidebar'),
      '#description' => $this->t('If checked, the field will be placed in the sidebar on entity forms.'),
    ];
    $element['use_details'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Wrap the toggles in a collapsed details container.'),
      '#default_value' => $this->getSetting('use_details'),
      '#description' => $this->t('If checked, the contents of the field will be placed inside a collapsed details container.'),
      '#states' => [
        'visible' => [
          'input[name$="[sidebar]"]' => ['checked' => FALSE],
        ],
      ],
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    if ($this->getSetting('sidebar')) {
      $summary['sidebar'] = $this->t('Use sidebar: Yes');
    }
    else {
      $summary['sidebar'] = $this->t('Use sidebar: No');

      if ($this->getSetting('use_details')) {
        $summary['use_details'] = $this->t('Use details container: Yes');
      }
      else {
        $summary['use_details'] = $this->t('Use details container: No');
      }
    }

    return $summary;
  }


  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    // In a multivalue field, the a form element is built for each value. If a set of form elements should be built with certain default values,that would not be accomplished here.

    $item = $items[$delta];
    // $default_tags = metatag_get_default_tags($items->getEntity());

    // Retrieve the values for each platform from the serialized array.
    $values = [];
    if (!empty($item->value)) {
      $values = unserialize($item->value, ['allowed_classes' => FALSE]);
    }

    // Make sure that this variable is always an array to avoid problems when
    // unserializing didn't work correctly and it as returned as FALSE.
    // @see https://www.php.net/unserialize
    if (!is_array($values)) {
      $values = [];
    }

    // If the "sidebar" option was checked on the field widget, put the form
    // element into the form's "advanced" group. Otherwise, let it default to
    // the main field area.
    $sidebar = $this->getSetting('sidebar');
    if ($sidebar) {
      $element['#group'] = 'advanced';
    }

    // If the "use_details" option was not checked on the field widget, put the
    // form element into normal container. Otherwise, let it default to a
    // detail's container.
    $details = $this->getSetting('use_details');
    if (!$sidebar && !$details) {
      $element['#type'] = 'container';
    }

    $element['#theme'] = 'social_post_controller_widget';

    // hide the last item, which will be removed in hook field_widget_multivalue_form_alter
    if (!isset($items[$delta]->platform)) {
      $element['#access'] = FALSE;
    }

    $element['platform_title'] = [
      '#type' => 'markup',
      '#markup' => $items[$delta]->platform,
    ];

    $element['platform'] = [
      // '#element_validate' => [[static::class, 'validatePlatforms']],
      '#type' => 'textarea',
      '#title' => 'platform',
      '#default_value' => $items[$delta]->platform,
      '#access' => FALSE,
    ];

    $element['message'] = [// ['default']['message'] = [
      '#element_validate' => [[static::class, 'validateMessage']],
      '#type' => 'textarea',
      '#title' => 'message',
      '#default_value' => $items[$delta]->message,
      '#platform' => $items[$delta]->platform,
    ];

    $element['status'] = [
      '#type' => 'checkbox',
      '#title' => 'status',
      '#default_value' => $items[$delta]->status,
    ];

    return $element;
  }

  /**
   * Validate the social post controller platform element.
   */
  public static function validateMessage($element, FormStateInterface $form_state) {
    $platform = $element['#platform'];
    $fieldStorageDefinition = \Drupal::service('entity_field.manager')->getFieldStorageDefinitions('node', 'microblog')['field_microblog_' . $platform] ?? NULL;
    $length = $fieldStorageDefinition->getSettings()['max_length'];
    // $form_state->setValueForElement($element, 'This is a hardcoded string');
  }


  /**
   *
   */
  public function getEnabledPlatforms($platforms) {

    foreach($platforms as $platform => $status) {
      if ($status['status'] === self::DISABLED) {
        unset($platforms[$platform]);
      } else {
        $platforms[$platform] = ($status['status'] === self::ON) ? TRUE : FALSE;
      }
    }
    return $platforms;
  }

}
