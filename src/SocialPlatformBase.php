<?php

namespace Drupal\social_post_controller;

use Drupal\Component\Plugin\PluginBase;

/**
 * A base class to help developers implement their own social platform plugins.
 *
 * @see \Drupal\social_post_controller\Annotation\SocialPlatform
 * @see \Drupal\social_post_controller\SocialPlatformInterface
 */
abstract class SocialPlatformBase extends PluginBase implements SocialPlatformInterface
{

  /**
   * {@inheritdoc}
   */
  public function label()
  {
    // Retrieve the @label property from the annotation and return it.
    return $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function description()
  {
    // Retrieve the @description property from the annotation and return it.
    return $this->pluginDefinition['description'];
  }

  /**
   * {@inheritdoc}
   */
  public function maxCharCount()
  {
    // Retrieve the @max_char_count property from the annotation and return it.
    return (int) $this->pluginDefinition['max_char_count'];
  }

}
