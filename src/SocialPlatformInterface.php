<?php

namespace Drupal\social_post_controller;

/**
 * Defines the required interface for all social platform plugins.
 *
 */
interface SocialPlatformInterface
{

  /**
   * Provide a label for the posting mechanism.
   *
   * @return string
   *   A string label of the mechanism for posting content to a third party social platform.
   */
  public function label();

  /**
   * Provide a description of the mechanism for posting content to a third party social platform.
   *
   * @return string
   *   A string description of the mechanism for posting content to a third party social platform.
   */
  public function description();

  /**
   * Define the maximum number of characters allowed on the target third party social platform.
   *
   * @return int
   *  The maximum number of characters allowed on the target third party social platform.
   */
  public function maxCharCount();

}
