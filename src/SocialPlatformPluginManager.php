<?php

namespace Drupal\social_post_controller;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\social_post_controller\Annotation\SocialPlatform;

/**
 * A plugin manager for social platform plugins.
 */
class SocialPlatformPluginManager extends DefaultPluginManager
{

  /**
   * Creates the discovery object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    // We replace the $subdir parameter with our own value.
    // This tells the plugin manager to look for SocialPlatform plugins in the
    // 'src/Plugin/SocialPlatform' subdirectory of any enabled modules. This also
    // serves to define the PSR-4 subnamespace in which social platform plugins will
    // live. Modules can put a plugin class in their own namespace such as
    // Drupal\{module_name}\Plugin\SocialPlatform\MySocialPlatformPlugin.
    $subdir = 'Plugin/SocialPlatform';

    $plugin_interface = SocialPlatformInterface::class;

    $plugin_definition_annotation_name = SocialPlatform::class;

    parent::__construct($subdir, $namespaces, $module_handler, $plugin_interface, $plugin_definition_annotation_name);

  }
}



